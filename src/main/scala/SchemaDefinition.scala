import sangria.schema.ObjectType
import sangria.schema._

object SchemaDefinition {

  val ConceptType = ObjectType("Concept", "a concept", fields[DaInterviewRepo, Concept](
    Field("id", StringType, None, resolve = _.value.id),
    Field("displayName", StringType, Some("dispay name"), resolve = _.value.displayName)
    //    Field("relateds", ListType(ConceptType), Some("related concepts"), resolve = _.value.relateds)
  ))

  val AnswerType = ObjectType("Answer", "an answer", fields[DaInterviewRepo, Answer](
    Field("keywords", ListType(StringType), Some("keywords"), resolve = _.value.keywords),
    Field("concept", OptionType(ConceptType), Some("concept"), resolve = _.value.concept)
  ))

  val QuestionType = ObjectType("Question", "a question", fields[DaInterviewRepo, Question](
    Field("id", StringType, None, resolve = _.value.id),
    Field("text", StringType, None, resolve = _.value.text),
    Field("answers", ListType(AnswerType), None, resolve = _.value.answers),
    Field("threshold", IntType, None, resolve = _.value.threshold),
    Field("unlockedBy", ListType(ConceptType), None, resolve = _.value.unlockedBy)
  ))

  val ID = Argument("id", StringType, description = "id of the entity")

  val Query = ObjectType("Query", fields[DaInterviewRepo, Unit](
    Field("QuestionById", QuestionType, arguments = ID :: Nil, resolve = c => c.ctx.getQuestionById(c arg ID).get),
    Field("question", ListType(QuestionType), arguments = Nil, resolve = c => c.ctx.getQuestions()),
    Field("concept", ListType(ConceptType), arguments = Nil, resolve = c => c.ctx.getConcepts())
  ))

  val SeriesSchema = Schema(Query)

}
