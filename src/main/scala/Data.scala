case class Question(
                   id: String,
                   text: String,
                   answers: List[Answer],
                   unlockedBy: List[Concept],
                   threshold: Int
                   )

case class Answer(
                 keywords: List[String],
                 concept: Option[Concept],
                 weight: Int
                 )

case class Concept(
                  id: String,
                  displayName: String,
                  relateds: List[Concept]
                  )
object Data {
  val concepts = List(
    Concept("java", "Java", relateds = List.empty),
    Concept("scala", "Scala", relateds = List.empty),
    Concept("angular", "Angular", relateds = List.empty),
    Concept("react", "React", relateds = List.empty)
  )
  val questions = List(
    Question("1", "what is the diff", answers = List(Answer(List("syntax"), Some(concepts(1)), weight = 2)), unlockedBy = List(concepts(0)), threshold = 2),
    Question("2", "what is the best", answers = List(Answer(List("syntax"), Some(concepts(2)), weight = 1)), unlockedBy = List(concepts(1)), threshold = 2),
    Question("3", "what is the worse", answers = List(Answer(List("syntax"), Some(concepts(3)), weight = 2)), unlockedBy = List(concepts(2)), threshold = 2)
  )
}

class DaInterviewRepo {
  import Data._
  def getQuestionById(id: String) : Option[Question] = questions.find(_.id == id)
  def getQuestions() : List[Question] = questions
  def getConcepts() : List[Concept] = concepts
}

