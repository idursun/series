import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import sangria.execution.{ErrorWithResolver, Executor, QueryAnalysisError}
import sangria.parser.QueryParser

import scala.io.StdIn
import sangria.marshalling.sprayJson._
import sangria.renderer.SchemaRenderer
import spray.json._

import scala.util.{Failure, Success}

object Main {

  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher
    val route = {
      (post & path("query")) {
        entity(as[String]) { request =>
          println("got request")
          val query = request
          println(query)

          QueryParser.parse(query) match {
            case Success(ast) =>
              val result = Executor.execute(SchemaDefinition.SeriesSchema, ast, new DaInterviewRepo)
              val response = result.map(OK -> _).recover {
                case error: QueryAnalysisError => BadRequest -> error.resolveError
                case error: ErrorWithResolver => InternalServerError -> error.resolveError
              }
              complete(response)
            case Failure(error) =>
              complete(BadRequest, JsObject("error" -> JsString(error.getMessage)))

          }
        }
      } ~
      get {
        path("schema") {
          complete(SchemaRenderer.renderSchema(SchemaDefinition.SeriesSchema))
        }

      }
    }

    val bindingFuture = Http().bindAndHandle(route, "localhost", 8888)

    println("starting server")
    StdIn.readLine()

    bindingFuture.flatMap(_.unbind()).onComplete(_ => system.terminate())
    println("stopping server")

  }
}

