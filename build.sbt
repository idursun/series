name := "sangria2"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
  "org.sangria-graphql" %% "sangria" % "1.0.0-RC5",
  "org.sangria-graphql" %% "sangria-spray-json" % "0.3.2",
  "com.typesafe.akka" %% "akka-http-core" % "10.0.1",
  "com.typesafe.akka" %% "akka-http" % "10.0.1",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.0.1" % "test",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.1",
  "com.typesafe.akka" %% "akka-http-jackson" % "10.0.1",
  "com.typesafe.akka" %% "akka-http-xml" % "10.0.1"
)
    